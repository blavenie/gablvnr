
## Compiler le site, depuis le code source

Site web réalisée grâce à Reveal.js

1. Récupérer le code source

```bash
git clone git@framagit.org/blavenie/gablvnr.git
cd gablvnr
```

2. Installer NodeJS v12 ou +
3. Installer les dépendances
```bash
npm install
```
4. Compiler le site :
```bash
npm run build
```

ou bien le lancer dans un navigateur :

```bash
npm start
```
